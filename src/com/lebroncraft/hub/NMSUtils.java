package com.lebroncraft.hub;


import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;

import com.google.common.collect.Lists;

import net.minecraft.server.v1_8_R2.EntityInsentient;
import net.minecraft.server.v1_8_R2.EntityTypes;


public class NMSUtils {
	
	private static List<Map<?, ?>> dataMaps = null;
	private static Method methodA = null;
	
	public static void registerEntity(String name, int id, Class<? extends EntityInsentient> customClass) {
		
		/*
		 * First, we make a list of all Maps in the EntityTypes class by looping through all fields.
		 * By creating a list of these maps we can easily modify them later on.
		 */
		
		try {
			if (dataMaps == null) {
				dataMaps = Lists.newArrayList();
				for (Field field : EntityTypes.class.getDeclaredFields()) {
					if (field.getType()
							.getSimpleName()
							.equals(Map.class.getSimpleName())) {
						field.setAccessible(true);
						dataMaps.add((Map<?, ?>) field.get(null));
					}
				}
			}
			/*
			 * since minecraft checks if an id has already been registered, we have to remove the
			 * old entity class before we can register our custom one. Map 0 is the map with names,
			 * and map 2 is the map with ids
			 */
			if (dataMaps.get(2)
					.containsKey(id)) {
					
				dataMaps.get(0)
						.remove(name);
				dataMaps.get(2)
						.remove(id);
			}
			
			/*
			 * now we call the method which adds the entity to the lists in the EntityTypes class,
			 * now we are actually 'registering' our entity
			 */
			if (methodA == null) {
				methodA = EntityTypes.class.getDeclaredMethod("a", Class.class, String.class, int.class);
				methodA.setAccessible(true);
			}
			
			methodA.invoke(null, customClass, name, id);
			
			// if (staticBiomeFields == null) {
			// staticBiomeFields = Lists.newArrayList();
			// for (Field field : BiomeBase.class.getDeclaredFields()) {
			// if (field.getType()
			// .getSimpleName()
			// .equals(BiomeBase.class.getSimpleName()) && field.get(null) != null) {
			// staticBiomeFields.add(field);
			// }
			// }
			// }
			//
			// if (biomeMetaLists == null) {
			// biomeMetaLists = Lists.newArrayList();
			// for (Field field : BiomeBase.class.getDeclaredFields()) {
			// if (field.getType()
			// .getSimpleName()
			// .equals(List.class.getSimpleName())) {
			// field.setAccessible(true);
			// biomeMetaLists.add(field);
			// }
			// }
			// }
			//
			// for (Field biome : staticBiomeFields) {
			// for (Field list : biomeMetaLists) {
			// @SuppressWarnings("unchecked")
			// List<BiomeMeta> metaList = (List<BiomeMeta>) list.get(biome.get(null));
			//
			//
			// for (BiomeMeta meta : metaList) {
			// Field clazz = BiomeMeta.class.getDeclaredFields()[0];
			// if (clazz.get(meta)
			// .equals(nmsClass)) {
			// clazz.set(meta, customClass);
			// }
			// }
			// }
			// }
			
		} catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException | NoSuchMethodException
				| SecurityException ex) {
			ex.printStackTrace();
		}
	}
}
