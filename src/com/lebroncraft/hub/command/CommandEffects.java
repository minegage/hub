package com.lebroncraft.hub.command;

import java.util.List;

import org.bukkit.entity.Player;

import com.lebroncraft.core.command.CommandBase;
import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.hub.HubManager;

public class CommandEffects 
		extends CommandBase {
	
	private HubManager hubManager;
	
	public CommandEffects(HubManager hubManager) {
		super(Rank.MEMBER, "effects");
		this.hubManager = hubManager;
	}
	
	@Override
	public void onCommand(Player player, List<String> args, Flags flags) {
		hubManager.toggleEffects(player);
	}
	
}
