package com.lebroncraft.hub;


import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;

import com.lebroncraft.core.CorePlugin;
import com.lebroncraft.core.block.BlockManager;
import com.lebroncraft.core.combat.CombatManager;
import com.lebroncraft.core.combat.DeathMessenger.DeathMessageMode;
import com.lebroncraft.core.command.CommandManager;
import com.lebroncraft.core.condition.VisibilityManager;
import com.lebroncraft.core.db.DBManager;
import com.lebroncraft.core.equippable.EquipManager;
import com.lebroncraft.core.event.EventManager;
import com.lebroncraft.core.menu.MenuManager;
import com.lebroncraft.core.move.MoveManager;
import com.lebroncraft.core.npc.NPCManager;
import com.lebroncraft.core.rank.RankManager;
import com.lebroncraft.core.server.ServerManager;
import com.lebroncraft.core.ticker.Ticker;
import com.lebroncraft.core.timer.Timer;
import com.lebroncraft.hub.entity.PortalMobManager;
import com.lebroncraft.hub.menu.HubMenu;
import com.lebroncraft.hub.stats.StatCache;


public class Hub
		extends CorePlugin {
		
	// Queue related stuff commented out for downtime update
	// private Queue queue;
	private MenuManager menuManager;
	private StatCache statCache;
	private HubManager hubManager;
	private EquipManager equipManager;
	private HubMenu hubMenu;
	private NPCManager npcManager;
	private DBManager dbManager;
	private CombatManager combatManager;
	private BlockManager blockManager;
	private PortalMobManager portalManager;
	
	@Override
	public void onEnable() {
		super.onEnable();
		
		// this.queue = new Queue(this, serverManager);
		this.menuManager = new MenuManager(this);
		this.statCache = new StatCache(this);
		this.hubManager = new HubManager(this, this);
		this.equipManager = new EquipManager(this, menuManager, moveManager);
		this.hubMenu = new HubMenu(this, serverManager, menuManager, equipManager, statCache, hubManager);
		this.npcManager = new NPCManager(this);
		this.dbManager = new DBManager(this);
		this.combatManager = new CombatManager(this);
		this.combatManager.getDeathMessenger().mode = DeathMessageMode.NONE;
		this.blockManager = new BlockManager(this);
		this.portalManager = new PortalMobManager(this);
		
		for (Player player : getServer().getOnlinePlayers()) {
			for (Player hidden : player.spigot()
					.getHiddenPlayers()) {
				player.showPlayer(hidden);
			}
		}
		
		for (Player player : getServer().getOnlinePlayers()) {
			PlayerJoinEvent event = new PlayerJoinEvent(player, null);
			getServer().getPluginManager()
					.callEvent(event);
		}
		
	}
	
	// @Override
	// public void onDisable() {
	// queue.getUpdateQueue()
	// .cancelAllTasks();
	// }
	
	@Override
	public CommandManager getCommandManager() {
		return commandManager;
	}
	
	@Override
	public Timer getCharger() {
		return timer;
	}
	
	@Override
	public ServerManager getServerManager() {
		return serverManager;
	}
	
	public MoveManager getMovement() {
		return moveManager;
	}
	
	@Override
	public VisibilityManager getVisibilityManager() {
		return visibilityManager;
	}
	
	@Override
	public EventManager getEventManager() {
		return eventManager;
	}
	
	// public Queue getQueue() {
	// return queue;
	// }
	//
	public MenuManager getMenuManager() {
		return menuManager;
	}
	
	public StatCache getStatCache() {
		return statCache;
	}
	
	public HubManager getHubManager() {
		return hubManager;
	}
	
	@Override
	public RankManager getRankManager() {
		return rankManager;
	}
	
	public EquipManager getEquipManager() {
		return equipManager;
	}
	
	public HubMenu getHubMenu() {
		return hubMenu;
	}
	
	@Override
	public Ticker getTicker() {
		return ticker;
	}
	
	public NPCManager getNpcManager() {
		return npcManager;
	}
	
	public BlockManager getFallingBlockManager() {
		return blockManager;
	}
	
	public DBManager getDbManager() {
		return dbManager;
	}
	
	public CombatManager getCombatManager() {
		return combatManager;
	}
	
	public PortalMobManager getPortalManager() {
		return portalManager;
	}
	
}
