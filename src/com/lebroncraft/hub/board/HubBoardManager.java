package com.lebroncraft.hub.board;


import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Team;

import com.lebroncraft.core.board.Board;
import com.lebroncraft.core.board.BoardManager;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.core.rank.RankManager;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;


public class HubBoardManager
		extends BoardManager {
		
	public HubBoardManager(JavaPlugin plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void giveBoard(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		
		Rank rank = RankManager.instance.getRank(player);
		
		// Sets prefix of joining player
		for (Board other : getPlayerBoards()) {
			Team team = other.getBoard()
					.getTeam(rank.getTeamName());
			team.addPlayer(player);
		}
		
		giveDefaultBoard(player);
	}
	
	public void giveDefaultBoard(Player player) {
		HubBoard board = new HubBoard(player);
		setBoard(player, board);
	}
	
	@EventHandler
	public void tickNews(TickEvent event) {
		if (event.getTick() != Tick.TICK_2) {
			return;
		}
		
		for (Board board : getBoards().values()) {
			if (board instanceof HubBoard) {
				( (HubBoard) board ).tickNews();
			}
		}
	}
	
}
