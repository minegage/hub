package com.lebroncraft.hub.board;


import java.util.List;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.lebroncraft.core.board.Board;
import com.lebroncraft.core.board.ObjectiveSide;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilUI;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.core.rank.RankManager;


public class HubBoard
		extends Board {
		
	public int newsIndex = 0;
	public int scrollIndex = 0;
	
	public HubBoard(Player player) {
		super();
		
		ObjectiveSide side = setSideObjective();
		
		RankManager rankManager = RankManager.instance;
		Rank rank = rankManager.getRank(player);
		
		side.setHeader(C.cBold + "Lebroncraft");
		side.setRow(15, "");
		side.setRow(14, C.cGreen + C.cBold + "Rank");
		side.setRow(13, rank.getFullName());
		side.setRow(12, "");
		side.setRow(11, C.cYellow + C.cBold + "News");
		side.setRow(10, "");
		side.setRow(9, "");
		side.setRow(8, C.cAqua + C.cBold + "IP");
		side.setRow(7, "lebroncraft.com");
		side.setRow(6, "");
		side.setRow(5, "------------------");
		
		UtilUI.createRankTeams(this);
		UtilUI.assignRankTeams(this);
	}
	
	private static final int WIDTH = 18;
	private static final String PADDING;
	private static List<String> news = Lists.newArrayList();
	
	static {
		StringBuilder spaces = new StringBuilder();
		for (int i = 0; i < WIDTH; i++) {
			spaces.append(" ");
		}
		PADDING = spaces.toString();
		
		addNews("Check out the open beta server!");
		addNews("We are in a development stage; stay tuned!");
		addNews("Have a suggestion? Let us know on the forums!");
	}
	
	private static void addNews(String message) {
		/* Add extra spaces at the start to give it a bit of delay */
		news.add("     " + PADDING + message + PADDING);
	}
	
	public void tickNews() {
		String message = news.get(newsIndex);
		
		// Go to next message if message section isn't big enough
		if (scrollIndex + WIDTH > message.length()) {
			scrollIndex = 0;
			
			if (++newsIndex >= news.size()) {
				newsIndex = 0;
			}
			
			message = news.get(newsIndex);
		}
		
		String display = message.substring(scrollIndex, scrollIndex + WIDTH);
		
		ObjectiveSide side = getSideObjective();
		side.updateRow(10, display);
		
		scrollIndex++;
	}
	
}
