
package com.lebroncraft.hub;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.common.util.UtilServer;
import com.lebroncraft.core.common.util.UtilSound;
import com.lebroncraft.core.common.util.UtilUI;
import com.lebroncraft.core.condition.VisibilityManager;
import com.lebroncraft.core.event.CustomDeathEvent;
import com.lebroncraft.core.module.PluginModule;
import com.lebroncraft.core.rank.Rank;
import com.lebroncraft.core.rank.RankManager;
import com.lebroncraft.core.ticker.TickEvent;
import com.lebroncraft.core.ticker.Ticker.Tick;
import com.lebroncraft.hub.board.HubBoardManager;
import com.lebroncraft.hub.command.CommandDebug;
import com.lebroncraft.hub.command.CommandEffects;


public class HubManager
		extends PluginModule {
		
	/* Players which have reduced visibility (can't see non-donators) */
	private Set<Player> vanishedOthers = new HashSet<>();
	
	private Hub hub;
	private HubBoardManager boardManager;
	
	private List<PotionEffect> effects = new ArrayList<>();
	
	public HubManager(JavaPlugin plugin, Hub hub) {
		super("Hub Manager", plugin);
		
		this.hub = hub;
		this.boardManager = new HubBoardManager(plugin);
		
		// File configFile = new File(plugin.getDataFolder(), "config.yml");
		// config = new ConfigHub(plugin, configFile);
		
		registerCommand(new CommandDebug(hub));
		// registerCommand(new CommandSpawn(config));
		// registerCommand(new CommandSetSpawn(config));
		registerCommand(new CommandEffects(this));
		
		effects.add(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 1));
		effects.add(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 1));
	}
	
	@EventHandler
	public void onTrade(InventoryOpenEvent event) {
		if (event.getInventory()
				.getType() == InventoryType.MERCHANT) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void preventStorm(WeatherChangeEvent event) {
		if (event.toWeatherState()) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void preventHunger(TickEvent event) {
		if (event.getTick() != Tick.MIN_1) {
			return;
		}
		
		for (Player player : plugin.getServer()
				.getOnlinePlayers()) {
			player.setFoodLevel(20);
			player.setSaturation(20F);
			player.setExhaustion(0F);
		}
	}
	
	@EventHandler
	public void handleJoin(PlayerJoinEvent event) {
		event.setJoinMessage(null);
		Player player = event.getPlayer();
		
		for (Player other : UtilServer.playersList()) {
			player.showPlayer(other);
		}
		
		// If this player is not a donator, hide them from players with the reduced player setting
		if (isVanishable(player)) {
			for (Player other : vanishedOthers) {
				other.hidePlayer(player);
			}
		}
		
		boolean allowFlight = RankManager.instance.hasPermission(player, Rank.MVP);
		player.setAllowFlight(allowFlight);
		
		if (player.hasPlayedBefore()) {
			for (PotionEffect effect : player.getActivePotionEffects()) {
				if (!effects.contains(effect)) {
					giveEffects(player);
					break;
				}
			}
		} else {
			giveEffects(player);
		}
		
		UtilUI.sendTabText(player, C.cBold + "Lebroncraft", "Check out " + C.sOut + "www.lebroncraft.com" + C.cWhite
				+ " for shop and forums!");
	}
	
	@EventHandler
	public void handleQuit(PlayerQuitEvent event) {
		event.setQuitMessage(null);
		vanishedOthers.remove(event.getPlayer());
	}
	
	@EventHandler
	public void handleDeath(CustomDeathEvent event) {
		event.setRespawnLocation(event.getKilled()
				.getWorld()
				.getSpawnLocation());
				
		// Prevent hurt sound from playing
		event.getCause()
				.setCancelled(true);
	}
	
	/* Effects handling */
	public void giveEffects(Player player) {
		for (PotionEffect effect : effects) {
			player.addPotionEffect(effect);
		}
	}
	
	public void removeEffects(Player player) {
		UtilEntity.clearPotionEffects(player);
	}
	
	public boolean hasEffects(Player player) {
		return player.getActivePotionEffects()
				.size() > 0;
	}
	
	public void toggleEffects(Player player) {
		if (hasEffects(player)) {
			removeEffects(player);
		} else {
			giveEffects(player);
		}
	}
	
	/* Vanish handling */
	
	public boolean isVanishable(Player player) {
		return !RankManager.instance.hasPermission(player, Rank.PRO);
	}
	
	public void vanishOthers(Player player) {
		for (Player other : getServer().getOnlinePlayers()) {
			if (isVanishable(other)) {
				player.hidePlayer(other);
			}
		}
		
		vanishedOthers.add(player);
		C.pMain(player, "Visibility", "Players are now " + C.sOut + "reduced");
	}
	
	public void unvanishOthers(Player player) {
		for (Player other : player.spigot()
				.getHiddenPlayers()) {
			player.showPlayer(other);
		}
		
		vanishedOthers.remove(player);
		C.pMain(player, "Visibility", "Players are now " + C.sOut + "visible");
	}
	
	/* General rules */
	
	@EventHandler
	public void onVoidDamage(EntityDamageEvent event) {
		Entity entity = event.getEntity();
		
		if (entity.getType() != EntityType.PLAYER) {
			return;
		}
		
		Player player = (Player) entity;
		
		if (event.getCause() == DamageCause.VOID) {
			UtilSound.playLocal(player, Sound.CAT_MEOW, 1F, 1F);
			event.setDamage(5000);
		} else {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void stopBlockBreak(BlockBreakEvent event) {
		Player player = event.getPlayer();
		if (!RankManager.instance.hasPermission(player, Rank.ADMIN)) {
			event.setCancelled(true);
			return;
		}
		
		if (player.getGameMode() != GameMode.CREATIVE) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void stopBlockPlace(BlockPlaceEvent event) {
		Player player = event.getPlayer();
		if (!RankManager.instance.hasPermission(player, Rank.ADMIN)) {
			event.setCancelled(true);
			return;
		}
		
		if (player.getGameMode() != GameMode.CREATIVE) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void stopBlockDamage(BlockDamageEvent event) {
		Player player = event.getPlayer();
		if (!RankManager.instance.hasPermission(player, Rank.ADMIN)) {
			event.setCancelled(true);
			return;
		}
		
		if (player.getGameMode() != GameMode.CREATIVE) {
			event.setCancelled(true);
		}
	}
	
	public VisibilityManager getVisibilityManager() {
		return hub.getVisibilityManager();
	}
	
	public HubBoardManager getBoardManager() {
		return boardManager;
	}
	
}
