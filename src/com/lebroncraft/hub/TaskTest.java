package com.lebroncraft.hub;

import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.module.PluginModule;

public class TaskTest 
		extends PluginModule {
	
	public TaskTest(JavaPlugin plugin) {
		super("Task Test", plugin);
	}
	
	@Override
	public void run() {
		d("running task test");
	}
	
}
