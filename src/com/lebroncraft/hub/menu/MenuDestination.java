package com.lebroncraft.hub.menu;


import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.lebroncraft.core.common.util.UtilItem;
import com.lebroncraft.core.menu.Menu;
import com.lebroncraft.core.menu.MenuManager;
import com.lebroncraft.core.menu.button.ButtonCommand;
import com.lebroncraft.core.menu.button.ButtonServer;
import com.lebroncraft.core.server.ServerManager;


public class MenuDestination
		extends Menu {
		
	public static final String RAW_NAME = "destination";
	
	private ServerManager serverManager;
	
	public MenuDestination(MenuManager manager, ServerManager serverManager) {
		super(manager, "Select your destination!", RAW_NAME, 3, "close");
		this.serverManager = serverManager;
		addComponents();
	}
	
	@Override
	public void addComponents() {
		// Removed for downscale update, will need in future
		// addQueue(getSlot(0, 2), "XP Wars", Material.EXP_BOTTLE);
		// addQueue(getSlot(2, 2), "Skywars", Material.FEATHER);
		// addQueue(getSlot(4, 2), "Predator", Material.COMPASS);
		// addQueue(getSlot(6, 2), "OITC TDM", Material.ARROW);
		// addQueue(getSlot(8, 2), "Survival Games", Material.COOKED_BEEF);
		
		
		addServer(getSlot(1, 1), "Kit PVP", "kitpvp", Material.IRON_SWORD);
		addServer(getSlot(3, 1), "Creative", "creative", Material.GRASS);
		addServer(getSlot(5, 1), "Skyblock", "skyblock", Material.SAPLING);
		addServer(getSlot(7, 1), "Beta", "beta", Material.REDSTONE);
		
		// ItemStack removeItem = UtilItem.create(Material.REDSTONE_BLOCK, ChatColor.BLUE + "Leave
		// queue");
		
		// ButtonCommand removeButton = new ButtonCommand("queue remove");
		// addButton(getSlot(4, 0), removeButton, removeItem);
	}
	
	@Override
	public void addItems(Player player, Inventory inventory) {
		// Do nothing
	}
	
	@SuppressWarnings("unused")
	private void addQueue(int slot, String name, Material material) {
		List<String> lore = new ArrayList<>();
		lore.add(ChatColor.GREEN + "Click " + ChatColor.WHITE + "to join " + ChatColor.YELLOW + name + ChatColor.WHITE + "!");
		
		ItemStack item = UtilItem.create(material, ChatColor.BLUE + name, lore);
		ButtonCommand button = new ButtonCommand("queue " + name);
		
		addButton(slot, button, item);
	}
	
	private void addServer(int slot, String displayName, String serverName, Material material) {
		List<String> lore = new ArrayList<>();
		lore.add(ChatColor.GREEN + "Click " + ChatColor.WHITE + "to play " + ChatColor.YELLOW + displayName + ChatColor.WHITE
				+ "!");
				
		ItemStack item = UtilItem.create(material, ChatColor.BLUE + displayName);
		ButtonServer button = new ButtonServer(serverManager, serverName);
		
		addButton(slot, button, item);
	}
	
}
