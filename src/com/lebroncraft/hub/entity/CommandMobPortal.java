package com.lebroncraft.hub.entity;


import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import com.lebroncraft.core.command.Flags;
import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.common.util.UtilPos;
import com.lebroncraft.core.mob.command.manager.CommandMobManager;
import com.lebroncraft.core.mob.command.manager.CreateToken;


public class CommandMobPortal
		extends CommandMobManager<PortalMobManager, PortalMob> {
		
	public CommandMobPortal(PortalMobManager manager) {
		super(manager, "mobportal");
	}
	
	
	@Override
	public CreateToken newToken() {
		return new PortalToken();
	}
	
	public static class PortalToken
			extends CreateToken {
			
		private String server;
		private String displayName;
		private Location location;
		
		@Override
		public boolean create(Player player, List<String> args, Flags flags) {
			if (args.size() < 1) {
				C.pMain(player, "Dest", "Please specify the server name");
				return false;
			}
			
			this.server = args.get(0);
			
			if (args.size() < 2) {
				C.pMain(player, "Dest", "Please specify the display name");
				return false;
			}
			
			this.displayName = args.get(1);
			this.location = player.getLocation();
			
			return true;
		}
		
		@Override
		public String create(LivingEntity entity) {
			entity.setNoDamageTicks(Integer.MAX_VALUE);
			entity.teleport(location);
			UtilEntity.removeAI(entity);
			
			String uid = entity.getUniqueId()
					.toString();
			String loc = UtilPos.serializeLocation(location);
			
			return server + ":" + displayName + ":" + uid + ":" + loc;
		}
		
	}
	
	
	
}
