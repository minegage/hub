package com.lebroncraft.hub.entity;


import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.LivingEntity;
import org.bukkit.plugin.java.JavaPlugin;

import com.lebroncraft.core.common.util.UtilPos;
import com.lebroncraft.core.mob.MobManager;
import com.lebroncraft.core.mob.MobType;


public class PortalMobManager
		extends MobManager<PortalMob> {
		
	public PortalMobManager(JavaPlugin plugin) {
		super("Portal Manager", plugin, "mobportals.txt");
		
		registerCommand(new CommandMobPortal(this));
	}
	
	public PortalMob create(String server, String displayName, MobType type, Location location) {
		LivingEntity entity = type.spawn(location);
		PortalMob destMob = new PortalMob(server, displayName, entity.getUniqueId(), location);
		
		destMob.load(entity);
		
		// TODO: Might be necessary
		// if (!destMob.isPostLoaded()) {
		// destMob.unload();
		// }
		
		return destMob;
	}
	
	@Override
	public PortalMob deserializeMob(World world, String serialized) {
		String[] split = serialized.split(":");
		
		String serverStr = split[0];
		String displayStr = split[1];
		String uidStr = split[2];
		String locStr = split[3];
		
		UUID uid = UUID.fromString(uidStr);
		Location loc = UtilPos.deserializeLocation(locStr, world);
		
		return new PortalMob(serverStr, displayStr, uid, loc);
	}
	
	@Override
	public String serializeMob(PortalMob mob) {
		return mob.server + ":" + mob.displayName + ":" + mob.getUid() + ":" + UtilPos.serializeLocation(mob.getPostLocation());
	}
	
	
	
	
	
}
