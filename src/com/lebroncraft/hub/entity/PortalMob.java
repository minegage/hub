package com.lebroncraft.hub.entity;


import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

import com.lebroncraft.core.common.C;
import com.lebroncraft.core.common.util.UtilEntity;
import com.lebroncraft.core.event.ClickEntityEvent;
import com.lebroncraft.core.mob.Mob;
import com.lebroncraft.core.server.ServerManager;



/**
 * Mob which is used to connect to servers
 */
public class PortalMob
		extends Mob {
		
	public String server;
	public String displayName;
	
	public PortalMob(String server, String displayName, UUID uid, Location location) {
		super(uid, location);
		this.server = server;
		this.displayName = displayName;
	}
	
	@Override
	public void load(LivingEntity entity) {
		super.load(entity);
		
		UtilEntity.removeAI(entity);
		UtilEntity.setSilent(entity, true);
		
		setArmourInvulnerable(true);
		
		setTag(C.cGreen + C.cBold + displayName);
	}
	
	@Override
	public void onClick(ClickEntityEvent event) {
		ServerManager.instance.connect(event.getClicker(), server);
	}
	
}
