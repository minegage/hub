#!/bin/bash

export MAVEN_OPTS="-Xms1024m -Xmx2048m"

mvn clean;
mvn package;

printf "Packaging complete. Package is located in the target/ directory.\n";

